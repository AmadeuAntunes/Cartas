﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cartas.game
{
    public interface Carta
    {
        string nome
        {
            get;
            set;
        }

        Cor cor
        {
            get;
            set;
        }

        string descricao
        {
            get;
            set;
        }

        int mana
        {
            get;
            set;
        }
    }
}
