﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cartas.game.Cartas
{
    public class Terreno : Carta
    {
        public Cor cor { get; set; }

        public string descricao { get; set; }

        public int mana { get; set; }

        public string nome { get; set; }
    }
}
