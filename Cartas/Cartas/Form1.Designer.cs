﻿namespace Cartas
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.fecharToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.baralho = new System.Windows.Forms.Button();
            this.iniciarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.carta1 = new System.Windows.Forms.Button();
            this.carta2 = new System.Windows.Forms.Button();
            this.carta3 = new System.Windows.Forms.Button();
            this.carta4 = new System.Windows.Forms.Button();
            this.carta5 = new System.Windows.Forms.Button();
            this.cartaMesa = new System.Windows.Forms.Button();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.ErrorImage = null;
            this.pictureBox1.Location = new System.Drawing.Point(-5, 347);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(969, 169);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.pictureBox2.ErrorImage = null;
            this.pictureBox2.Location = new System.Drawing.Point(0, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(960, 351);
            this.pictureBox2.TabIndex = 1;
            this.pictureBox2.TabStop = false;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(955, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.iniciarToolStripMenuItem,
            this.fecharToolStripMenuItem});
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(59, 20);
            this.toolStripMenuItem1.Text = "Opções";
            // 
            // fecharToolStripMenuItem
            // 
            this.fecharToolStripMenuItem.Name = "fecharToolStripMenuItem";
            this.fecharToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.fecharToolStripMenuItem.Text = "Fechar";
            this.fecharToolStripMenuItem.Click += new System.EventHandler(this.fecharToolStripMenuItem_Click);
            // 
            // baralho
            // 
            this.baralho.AutoSize = true;
            this.baralho.BackColor = System.Drawing.Color.Transparent;
            this.baralho.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("baralho.BackgroundImage")));
            this.baralho.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.baralho.Cursor = System.Windows.Forms.Cursors.Default;
            this.baralho.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.baralho.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.baralho.ForeColor = System.Drawing.SystemColors.ControlDark;
            this.baralho.Location = new System.Drawing.Point(22, 89);
            this.baralho.Name = "baralho";
            this.baralho.Size = new System.Drawing.Size(120, 199);
            this.baralho.TabIndex = 3;
            this.baralho.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.baralho.UseMnemonic = false;
            this.baralho.UseVisualStyleBackColor = true;
            this.baralho.Click += new System.EventHandler(this.baralho_Click);
            // 
            // iniciarToolStripMenuItem
            // 
            this.iniciarToolStripMenuItem.Name = "iniciarToolStripMenuItem";
            this.iniciarToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.iniciarToolStripMenuItem.Text = "Iniciar";
            this.iniciarToolStripMenuItem.Click += new System.EventHandler(this.iniciarToolStripMenuItem_Click);
            // 
            // carta1
            // 
            this.carta1.Location = new System.Drawing.Point(189, 357);
            this.carta1.Name = "carta1";
            this.carta1.Size = new System.Drawing.Size(105, 154);
            this.carta1.TabIndex = 4;
            this.carta1.Text = "button1";
            this.carta1.UseVisualStyleBackColor = true;
            this.carta1.Click += new System.EventHandler(this.carta1_Click);
            // 
            // carta2
            // 
            this.carta2.Location = new System.Drawing.Point(300, 357);
            this.carta2.Name = "carta2";
            this.carta2.Size = new System.Drawing.Size(105, 154);
            this.carta2.TabIndex = 5;
            this.carta2.Text = "button2";
            this.carta2.UseVisualStyleBackColor = true;
            this.carta2.Click += new System.EventHandler(this.carta2_Click);
            // 
            // carta3
            // 
            this.carta3.Location = new System.Drawing.Point(411, 357);
            this.carta3.Name = "carta3";
            this.carta3.Size = new System.Drawing.Size(105, 154);
            this.carta3.TabIndex = 6;
            this.carta3.Text = "button3";
            this.carta3.UseVisualStyleBackColor = true;
            this.carta3.Click += new System.EventHandler(this.carta3_Click);
            // 
            // carta4
            // 
            this.carta4.Location = new System.Drawing.Point(522, 357);
            this.carta4.Name = "carta4";
            this.carta4.Size = new System.Drawing.Size(105, 154);
            this.carta4.TabIndex = 7;
            this.carta4.Text = "button4";
            this.carta4.UseVisualStyleBackColor = true;
            this.carta4.Click += new System.EventHandler(this.carta4_Click);
            // 
            // carta5
            // 
            this.carta5.Location = new System.Drawing.Point(633, 357);
            this.carta5.Name = "carta5";
            this.carta5.Size = new System.Drawing.Size(105, 154);
            this.carta5.TabIndex = 8;
            this.carta5.Text = "button5";
            this.carta5.UseVisualStyleBackColor = true;
            this.carta5.Click += new System.EventHandler(this.carta5_Click);
            // 
            // cartaMesa
            // 
            this.cartaMesa.Location = new System.Drawing.Point(399, 134);
            this.cartaMesa.Name = "cartaMesa";
            this.cartaMesa.Size = new System.Drawing.Size(105, 154);
            this.cartaMesa.TabIndex = 9;
            this.cartaMesa.Text = "button3";
            this.cartaMesa.UseVisualStyleBackColor = true;
            this.cartaMesa.Click += new System.EventHandler(this.cartaMesa_Click);
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(79, 44);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(804, 23);
            this.progressBar1.TabIndex = 10;
            this.progressBar1.Value = 100;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(955, 513);
            this.Controls.Add(this.progressBar1);
            this.Controls.Add(this.cartaMesa);
            this.Controls.Add(this.carta5);
            this.Controls.Add(this.carta4);
            this.Controls.Add(this.carta3);
            this.Controls.Add(this.carta2);
            this.Controls.Add(this.carta1);
            this.Controls.Add(this.baralho);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MainMenuStrip = this.menuStrip1;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Jogo das Cartas";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem fecharToolStripMenuItem;
        private System.Windows.Forms.Button baralho;
        private System.Windows.Forms.ToolStripMenuItem iniciarToolStripMenuItem;
        private System.Windows.Forms.Button carta1;
        private System.Windows.Forms.Button carta2;
        private System.Windows.Forms.Button carta3;
        private System.Windows.Forms.Button carta4;
        private System.Windows.Forms.Button carta5;
        private System.Windows.Forms.Button cartaMesa;
        private System.Windows.Forms.ProgressBar progressBar1;
    }
}

