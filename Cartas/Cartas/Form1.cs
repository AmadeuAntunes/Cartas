﻿using Cartas.game;
using Cartas.game.Cartas;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cartas
{
    public partial class Form1 : Form
    {
        List<Button> listaCartas = new List<Button>();
       
       
        public Form1()
        {
          
            InitializeComponent();
            baralho.Visible = false;
            cartaMesa.Visible = false;
            listaCartas.Add(carta1);
            listaCartas.Add(carta2);
            listaCartas.Add(carta3);
            listaCartas.Add(carta4);
            listaCartas.Add(carta5);
            progressBar1.Visible = false;
            foreach (var Cartas in listaCartas)
            {
                Cartas.Visible = false;
            }
        
        }

        private void fecharToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
          
        }

        private void iniciarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            baralho.Visible = true;
            progressBar1.Visible = true;
        }

        private void baralho_Click(object sender, EventArgs e)
        {
            bool cartasoff = true;
            foreach (var Cartas in listaCartas)
            {
                cartasoff = false;
                if (Cartas.Visible == false)
                {
                    cartasoff = true;
                }
               
            }
            if (cartasoff == true)
            {
                foreach (var Cartas in listaCartas)
                {
                    Cartas.Visible = true;

                }
            }
        
        }

        private void carta1_Click(object sender, EventArgs e)
        {
            listaCartas[0].Visible = false;
        }

        private void carta2_Click(object sender, EventArgs e)
        {
            listaCartas[1].Visible = false;
        }

        private void carta3_Click(object sender, EventArgs e)
        {
            listaCartas[2].Visible = false;
        }

        private void carta4_Click(object sender, EventArgs e)
        {
            listaCartas[3].Visible = false;
        }

        private void carta5_Click(object sender, EventArgs e)
        {
            listaCartas[4].Visible = false;
        }

        private void cartaMesa_Click(object sender, EventArgs e)
        {

        }
    }
}
